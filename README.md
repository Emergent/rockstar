# Rockstar

This is a bucket for any Rockstar code I write.

You can download a Rockstar compiler, or better yet head over to
https://codewithrockstar.com/online and just paste the code there.

I doubt there will be much here; I have some neat ideas for things to do with
this, but time likely won't allow it.